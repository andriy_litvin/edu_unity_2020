﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameHiddenObject : MonoBehaviour
{
    public List<string> objectsName;  // список всіх предметів
    public List<string> objectsFind;  // список предметів які зараз потрібно знайти
    int score = 0;                    // рахунок
    float timerGame = 0;              // таймер тривалості гри
    float timerTask = 0;              // таймер на виконання завдання
    Coroutine corGame, corTask;       // посилання на корутини
    
    void Start()
    {
        HiddenObject[] temp = GetComponentsInChildren<HiddenObject>();
        foreach (var item in temp)
        {
            objectsName.Add(item.name); 
        }
        timerGame = objectsName.Count * 10;
        NextObjectsToFind();
        corGame = StartCoroutine(TimerGame());
        corTask = StartCoroutine(TimerTask());
    }

    void NextObjectsToFind()
    {
        timerTask = 0;
        if (objectsName.Count == 0)
        {
            GameOver(true);
            return;
        }

        for (int i = 0; i < 2; i++)
        {
            if (objectsName.Count > 0)
            {
                int random = Random.Range(0, objectsName.Count);
                objectsFind.Add(objectsName[random]);
                objectsName.RemoveAt(random);
            }
        }
        PrintObjectsFind();
    }

    void PrintObjectsFind()
    {
        string mess = "Find objects: ";
        foreach (var item in objectsFind)
        {
            mess += "\t\t" + item;
        }
        print(mess);
    }

    void GameOver(bool isWinner)
    {
        string mess = "Game over, you ";
        if (isWinner)
        {
            mess += "winner\nScore: " + score;       
        }
        else
        {
            mess += "loser";
        }
        print(mess);
        StopCoroutine(corGame);
        StopCoroutine(corTask);
    }

    IEnumerator TimerGame()
    {
        while (timerGame>0)
        {
            yield return null;
            timerGame -= Time.deltaTime;
        } 
        GameOver(false);
    }

    IEnumerator TimerTask()
    {
        while (true)
        {
            yield return null;
            timerTask += Time.deltaTime;
        }
    }

    public bool IsFind(string name)
    {
        foreach (var item in objectsFind)
        {
            if (item == name)
                return true;
        }
        return false;
    }

    public void AddScore()
    {
        if (timerTask < 4)
            score += 3;
        else if (timerTask < 8)
            score += 2;
        else
            score++;
        print("Score: " + score + "\nTime left: " + timerGame);
    }

    public void DeleteObject(HiddenObject obj)
    {
        objectsFind.Remove(obj.name);
        Destroy(obj.gameObject);
        if (objectsFind.Count == 0)
        {
            NextObjectsToFind();
        }
    }
}
