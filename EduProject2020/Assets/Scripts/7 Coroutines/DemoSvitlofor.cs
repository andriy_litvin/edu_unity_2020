﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DemoSvitlofor : MonoBehaviour
{
    public SpriteRenderer[] lamps;
    Color defColor;

    void Start()
    {
        defColor = lamps[0].color;
        StartCoroutine(SvitloforAnim());
    }

    IEnumerator SvitloforAnim()
    {
        while (true)
        {      
            lamps[2].color = Color.green;
            yield return new WaitForSeconds(3);

            for (int i = 0; i < 3; i++)
            {
                lamps[2].color = defColor;
                yield return new WaitForSeconds(0.3f);
                lamps[2].color = Color.green;
                yield return new WaitForSeconds(0.3f);
            }

            lamps[2].color = defColor;
            lamps[0].color = Color.red;
            yield return new WaitForSeconds(3);

            for (int i = 0; i < 3; i++)
            {
                lamps[0].color = defColor;
                yield return new WaitForSeconds(0.3f);
                lamps[0].color = Color.red;
                yield return new WaitForSeconds(0.3f);
            }

            lamps[0].color = defColor;    
        }
    }

}
