﻿using System.Collections;
using UnityEngine;

public class NPCPlatformer : MonoBehaviour
{
    public float speed = 1;        // швидкість руху NPC
    public LayerMask layerGround;  // шар на якому знаходиться земля/платформа
    public Rigidbody2D bullet;     // префаб кулі 
    Animator anim;                 // посилання на компонент Animator поточного об'єкта
    Rigidbody2D rb;                // посилання на компонент Rigidbody2D поточного об'єкта
    SpriteRenderer sr;             // посилання на компонент SpriteRenderer поточного об'єкта
    float move=1;                  // напрямок руху 1 - вправо, -1 - вліво
    bool isRight = true;           // напрямок руху true - вправо, false - вліво
    Transform playerTrans;         // посилання на Transform персонажа
    float timerBulet = 0.5f;       // таймер для стрільби
    bool isShot = true;            // ознака того, що можна стріляти
    
    // ініціалізація посилань на компоненти, встановлення початкових параметрів, запуск корутини
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        anim.SetBool("ground", true);
        playerTrans = PlayerControllerPlat.sing.transform;
        StartCoroutine(TimeShot());
        SystemNPC.sing.AddCountNPC();
    }
    
    // перевірка на те чи не закінчилась платформа, якщо так то НПС повертається та йде в інший бік
    void FixedUpdate()
    {
        if (isRight && !Physics2D.OverlapCircle(transform.position + new Vector3(0.8f, -0.8f, 0), 0.1f, layerGround))
        {
            isRight = false;
            sr.flipX = true;
            move = -1;
        }
        else if (!isRight && !Physics2D.OverlapCircle(transform.position + new Vector3(-0.8f, -0.8f, 0), 0.1f, layerGround))
        {
            isRight = true;
            sr.flipX = false;
            move = 1;
        }
        anim.SetFloat("xSpeed", 1);
        rb.velocity = new Vector2(move * speed, rb.velocity.y);      
    }

    // реалізація стрільби НПС з перевіркою на те, де знаходиться персонаж, якщо перед ним, то стріляє з інтервалом
    private void Update()
    {
        if (isShot && Vector3.Distance(transform.position, playerTrans.position)<7)
        {
            if (Mathf.Abs(transform.position.y - playerTrans.position.y) < 1)
            {
                if (isRight && transform.position.x - playerTrans.position.x < 0)
                {
                    Rigidbody2D newBullet = Instantiate(bullet, transform.position+new Vector3(0.4f*move, 0, 0), Quaternion.identity);
                    newBullet.AddForce(new Vector2(500, 0));
                }
                else if (!isRight && transform.position.x - playerTrans.position.x > 0)
                {
                    Rigidbody2D newBullet = Instantiate(bullet, transform.position + new Vector3(0.4f * move, 0, 0), Quaternion.identity);
                    newBullet.AddForce(new Vector2(-500, 0));
                }
                isShot = false;
                timerBulet = 0.5f;
            }
        }    
    }

    // корутина, що виконує роль таймера, забезпечує проміжок в часі між вистрілами
    IEnumerator TimeShot()
    {
        while (true)
        {
            yield return null;
            timerBulet -= Time.deltaTime;
            if (timerBulet < 0)
            {
                isShot = true;
            }
        }
    }

    // якщо виникає колізія НПС з кулею персонажа, то видаляємо об'єкт НПС
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            SystemNPC.sing.RemoveNPC();
            Destroy(gameObject);        
        }
    }
}
