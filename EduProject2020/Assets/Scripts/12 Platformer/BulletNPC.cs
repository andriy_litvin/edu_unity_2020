﻿using UnityEngine;

public class BulletNPC : MonoBehaviour
{
    // видалення кулі НПС при колізії з будь яким об'єктом
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Destroy(gameObject);
    }

    // видалення кулі НПС при виході з області видимості всіх камер
    private void OnBecameInvisible()
    {
        Destroy(gameObject);    
    }

}
