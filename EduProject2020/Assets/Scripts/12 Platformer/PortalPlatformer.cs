﻿using UnityEngine;

public class PortalPlatformer : MonoBehaviour
{
    public int idLoadScene;   // id рівня який треба завантажити
    public Transform voronka; // посилання на дочірній префаб, для його обертання при активації порталу 
    bool isActiv=false;       // ознака активації потралу
    
    // якщо портал активний крутимо воронку
    void Update()
    {
        if (isActiv)
        {
            voronka.Rotate(0, 0, 10);
            if (Input.GetKeyDown(KeyCode.F))
            {

                UnityEngine.SceneManagement.SceneManager.LoadScene(idLoadScene);
            }
        }
    }

    // якщо в зону тригера заходить персонаж з трьома алмазами, то активуємо портал
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            if (collision.GetComponent<PlayerControllerPlat>().countAlmaz == 3)
            {
                isActiv = true;
            }
        }
    }
}
