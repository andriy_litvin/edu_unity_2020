﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DnoSens : MonoBehaviour
{
    public Transform restartPos;   // посилання на маркер точки старту

    // якщо відбувається колізія з гравцем, то він переміщується в стартову точку
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.transform.position = restartPos.position;
        }
    }


}
