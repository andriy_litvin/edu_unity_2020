﻿using UnityEngine;

public class PlayerControllerPlat : MonoBehaviour
{
    public float speed = 1;                   // швидкість руху
    public LayerMask layerGround;             // шар на якому знаходиться земля/платформа
    public Rigidbody2D bullet;                // префаб кулі 
    public int countAlmaz = 0;                // кількість алмазів
    public int hp = 30;                       // кількість життя 
    private static PlayerControllerPlat _sing;  // статична змінна для створення простого сінглетона
    Animator anim;                            // посилання на компонент Animator поточного об'єкта
    Rigidbody2D rb;                           // посилання на компонент Rigidbody2D поточного об'єкта
    SpriteRenderer sr;                        // посилання на компонент SpriteRenderer поточного об'єкта
    float move;                               // напрямок руху 1 - вправо, -1 - вліво
    bool isRight = true;                      // напрямок руху true - вправо, false - вліво
    bool isGround = false;                    // ознака того, що гравець на землі/платформі

    ParallaxPlatformor par;
    Transform cam;
    Vector3 deltaPosCam;
    Vector3 newPosCam;
    float posX, posY;

    //public GUIPlatformer gui;
    public UIPlatformer ui;

    public static PlayerControllerPlat sing
    {
        get
        {
            if (!_sing)
            {
                _sing = FindObjectOfType<PlayerControllerPlat>();
                DontDestroyOnLoad(_sing.gameObject);
            }
            return _sing;
        }
    }

    // ініціалізація сінглтона
    void Awake()
    {
        if (!_sing)
        {
            _sing = this;
            DontDestroyOnLoad(_sing.gameObject);
        }
        else if (this != _sing)
        {
            Destroy(this.gameObject);
        }
    }

    private void OnLevelWasLoaded(int level)
    {
        //Start();
        transform.position = GameObject.Find("StartPoint").transform.position;
    }

    // ініціалізація посилань на компоненти
    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();

        cam = Camera.main.transform;
        par = cam.GetComponentInChildren<ParallaxPlatformor>();
        deltaPosCam = cam.position - transform.position;
        posX = transform.position.x;
        posY = transform.position.y;

        //gui.InitGUI(hp);
        ui.InitUI(hp);
    }

    // реалізація руху персонажа та стрибків
    private void FixedUpdate()
    {
        isGround = Physics2D.OverlapCircle(transform.position + new Vector3(0, -0.7f, 0), 0.1f, layerGround);
        anim.SetBool("ground", isGround);
        anim.SetFloat("ySpeed", rb.velocity.y);
        if (!isGround)
            return;

        move = Input.GetAxis("Horizontal");
        anim.SetFloat("xSpeed", Mathf.Abs(move));
        rb.velocity = new Vector2(move * speed, rb.velocity.y);
        if (move > 0 && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
        else if (move < 0 && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }              
    }

    // ініціалізація стрибка, стрільба
    void Update()
    {
        newPosCam = transform.position + deltaPosCam;
        cam.position = newPosCam;
        par.run = (transform.position.x - posX) * 15f;
        posX = transform.position.x;

        par.jamp = transform.position.y - posY;
        posY = transform.position.y;

        if (isGround && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(new Vector2(0, 400));
        }
        if (Input.GetKeyDown(KeyCode.Return))
        {
            Rigidbody2D newBullet = Instantiate(bullet, transform.position, Quaternion.identity);
            if (isRight)
            {
                newBullet.AddForce(new Vector2(500, 0));
            }
            else
            {
                newBullet.AddForce(new Vector2(-500, 0));  
            }
        }
    }

    // коли гравець потрапляє в зону тригера бонуса, то виконується його підбирання
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bonus")
        {
            countAlmaz++;
            //gui.SetCountAlmaz(countAlmaz);
            ui.SetCountAlmaz(countAlmaz);
            Destroy(collision.gameObject);
        }
    }

    // якщо виконується колізія гравця з кулею НПС, то у гравця віднімаємо 10 очок життя та перевіряється залишок
    // якщо життя закінчилось, то виконуємо дію смерті
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "BulletNPC")
        {
            hp -= 10;
            //gui.SetHP(hp);
            ui.SetHP(hp);
            if (hp <= 0)
            {
                transform.Rotate(0,0, 90);
                Camera.main.transform.Rotate(0, 0, -90);
                transform.position -= new Vector3(0, 0.7f, 0);
                anim.SetTrigger("isDeath");
                sr.color = new Color(0.5f, 0.5f, 0.5f, 0.5f);
                rb.simulated = false;
                GetComponent<CapsuleCollider2D>();
                this.enabled = false;
            }
        }
    }
}
