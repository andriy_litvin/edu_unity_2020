﻿using UnityEngine;

public class Arrays2 : MonoBehaviour
{
    void Start()
    {
        //int[,] a = new int[5,4];
        //int[,,] b = new int[5, 4, 3];
        //int[,] c = { {3,5,7,2}, {8,3,6,3}, {5,2,7,5} }; // 3x4

        #region proba
        /*for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                a[i, j] = Random.Range(1, 10);
            }
        }

        string str = "";
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                str += a[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
        */
        #endregion
        /*
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    b[i, j, k] = Random.Range(1, 10);
                }
            }
        }

        string str = "";
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 4; j++)
            {
                for (int k = 0; k < 3; k++)
                {
                    str += b[i, j, k] + " ";
                }
                str += "\n";
            }
            str += "\n";
        }
        print(str);
        */

        /*int[][] d = new int[3][];
        d[0] = new int[3];
        d[1] = new int[5];
        d[2] = new int[4];
        */
        /*
        int[][] d2 = new int[3][]; // Ініціалізація основного масиву
        int[] p = { 3, 5, 4 }; // Допоміжний масив для збереження довжини кожного з підмасивів 
        // Ініціалізація підмасивів
        for (int i = 0; i < 3; i++)
        {
            d2[i] = new int[p[i]];
        }

        // Заповнення
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < p[i]; j++)
            {
                d2[i][j] = Random.Range(1, 10);
            }
        }

        // Вивід
        string str = "";
        for (int i = 0; i < 3; i++)
        {
            for (int j = 0; j < p[i]; j++)
            {
                str += d2[i][j] + " ";
            }
            str += "\n";
        }
        print(str);
        */

        // -------- 2 --------
        /*int n = Random.Range(3, 10);
        int m = Random.Range(3, 10);
        int[,] array = new int[n,m];
        RandomArray(array, n, m);
        PrintArray(array, n, m);
        int suma = 0;
        for(int i=0; i<n; i++)
        {
            for (int j = 0; j < m; j++)
            {
                suma += array[i, j];
            }
        }
        print(suma);*/

        // ------- 3 --------
        /*
        int n = Random.Range(3, 10);
        int[][] array = new int[n][];
        int[] arrayLen = new int[n];
        for (int i = 0; i < n; i++)
        {
            arrayLen[i] = Random.Range(1,10);
        }

        for (int i = 0; i < n; i++)
        {
            array[i] = new int[arrayLen[i]];
        }
        */

        // ------ 5a -------
        /*int[,] arr = new int[7, 7];
        for (int i = 0; i < 7; i++)
        {
            arr[i, i] = 1;
            arr[i, 6 - i] = 1;
        }
        PrintArray(arr, 7, 7);
        */

        // ------ 5b -------
        /*int[,] arr = new int[7, 7];
        for (int i = 0; i < 4; i++)
        {
            for (int j = i; j < 7 - i; j++)
            {
                arr[i, j] = 1;
                arr[6 - i, j] = 1;
            }
        }
        PrintArray(arr, 7, 7);
        */

        // -------- 9 ---------
        /*int[,] arr = new int[9, 12];
        RandomArray(arr, 9, 12);
        PrintArray(arr, 9, 12);
        int povtor = 0;
        foreach(int i in arr)
        {
            povtor = 0;
            foreach (int j in arr)
            {
                if(i==j)
                {
                    povtor++;
                    if (povtor > 1)
                    {
                        print("Є однакові елементи");
                        break;
                    }
                }
            }
            if (povtor > 1)
            {                
                break;
            }
        }
        if (povtor <= 1)
        {
            print("Немає однакових елементів");
        }*/

        // ------- 12 -------
        /*int[,] arr = new int[7, 7];
        RandomArray(arr, 7, 7);
        PrintArray(arr, 7, 7);
        int suma1 = 0;
        int suma2 = 0;
        int suma3 = 0;
        int suma4 = 0;
        for (int i = 0; i < 3; i++)
        {
            for (int j = i + 1; j < 6 - i; j++)
            {
                //Debug.LogFormat("{0}:{1}", i, j);
                suma1 += arr[i, j];
                suma2 += arr[5-i,j];
                suma3 += arr[j, i];
                suma4 += arr[j, 5 - i];
            }
        }
        print("-1-  " + suma1);
        print("-2-  " + suma2);
        print("-3-  " + suma3);
        print("-4-  " + suma4);
        */

        // ------- 6a --------
        /*int[,] arr = new int[5, 5];
        RandomArray(arr, 5, 5);
        PrintArray(arr, 5, 5);

        int max = int.MinValue;
        int rowMax = 0;

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                if(arr[i,j] > max)
                {
                    max = arr[i, j];
                    rowMax = i;
                    print(max);
                }
            }
        }

        string str = "";
        for(int i=0; i<5; i++)
        {
            str += arr[rowMax, i] + " ";
        }
        print(str);
        */

        // -------- 6c --------
        int[,] arr = new int[5, 5];
        RandomArray(arr, 5, 5);
        PrintArray(arr, 5, 5);

        int rowRepeat = 0; // рядок з максимальною кількістю повторів
        int countRepeat = 0; // максимальна кількість повторів
        int tempRepeat = 0; // поточна кількість повторів

        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5; j++)
            {
                tempRepeat = 0;
                for (int k = 0; k < 5; k++)
                {
                    if (arr[i, j] == arr[i, k])
                    {
                        tempRepeat++;
                    }
                }
                if (tempRepeat > countRepeat)
                {
                    countRepeat = tempRepeat;
                    rowRepeat = i;
                }
            }
        }

        string str = "";
        for (int i = 0; i < 5; i++)
        {
            str += arr[rowRepeat, i] + " ";
        }
        print(str);

    }

    // Метод заповнення масиву випадковими значеннями
    public void RandomArray(int[,] arrayInt, int arrayN, int arrayM)
    {
        for (int i = 0; i < arrayN; i++)
        {
            for (int j = 0; j < arrayM; j++)
            {
                arrayInt[i, j] = Random.Range(1, 10);
            }
        }
        arrayN = 0;
    }

    // Метод друку масиву
    private void PrintArray(int[,] arrayInt, int arrayN, int arrayM)
    {
        string str = "";
        for (int i = 0; i < arrayN; i++)
        {
            for (int j = 0; j < arrayM; j++)
            {
                str += arrayInt[i, j] + " ";
            }
            str += "\n";
        }
        print(str);
    }

}
