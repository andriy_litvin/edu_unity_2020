﻿using UnityEngine;

public class Loops : MonoBehaviour
{
    
    void Start()
    {
        // 0..10
        /*for (int i = 0; i <= 10; i++)
        {
            print(i);
        }*/

        // парні з 2 до 20
        /*for (int i = 2; i <= 20; i += 2)
        {
            print(i);
        }*/

        // непарні з 19 до 1
        /*for (int i = 19; i>=1; i-=2)
        {
            print(i);
        }*/

        // З проміжку чисер від 1 до 100
        // вибрати кратні 7 або 9
        /*for (int i = 1; i <= 100; ++i)
        {
            if (i % 7 == 0 || i % 9 == 0)
            {
                print(i);
            }
        }*/

        // 1..10
        /*int i = 1;
        while(i<=10)
        {
            print(i);
            i++;
        }*/

        // 20..10
        /*int i = 20;
        do
        {
            print(i);
            i--;
        }
        while (i > 9);*/

        // Цикл перебирає випадкові числа від 1 до 30
        // якщо число парне ми його пропускаємо
        // якщо число більше 20 виходимо з циклу
        /*int num;
        while (true)
        {
            num = Random.Range(1, 31);
            print("num = " + num);
            if (num > 20)
            {
                break;
            }
            else if (num % 2 == 0)
            {
                continue;
            }
            print("*** "+num+" ***");
        }*/

        // ----- 1 -----
        /*int n = Random.Range(2, 10);
        string str = "";
        for(int m=2; m<=9; m++)
        {
            str+=n+" * "+m+" = "+(n*m)+"\n";
        }
        print(str);
        */

        /*string str = "";
        for (int i = 2; i <= 9; i++)
        {
            for (int j = 2; j <= 5; j++)
            {
                str+= j + " * " + i + " = " + (i * j) + "\t";
            }
            str += "\n";
        }
        str += "\n";
        for (int i = 2; i <= 9; i++)
        {
            for (int j = 6; j <= 9; j++)
            {
                str += j + " * " + i + " = " + (i * j) + "\t";
            }
            str += "\n";
        }
        print(str);*/

        // ----- 2 ------
        /*int n = 0;
        int m = 4;
        int suma = 0;
        for(int i=n; i<=m; i++)
        {
            suma += i; 
        }
        print(suma);*/

        // ------ 6 -------
        /*int n = 2;
        int m = 5;
        int suma = 0;
        for(int i=1; i<=m; i++)
        {
            suma += (int)Mathf.Pow(i, n);
        }
        print(suma);
        */

        // ------- 7 -------
        /*int suma = 0;
        for (int i = 0; i < 10; i++)
        {
            suma += Random.Range(-100,100);
        }

        float serAref = suma / 10f;
        print(serAref);
        if (serAref > 20)
        {
            print("Середнє арефметичне більше за 20");
        }
        else
        {
            print("Середнє арефметичне не більше за 20");
        }*/

        // --------- 12 ---------
        /*float suma = 0;
        for (int i=2; i<=10; i++)
        {
            suma += i / (i + 1f);
        }
        print(suma);
        */

        // ------- 15 -------
        /*string str = ""; 
        for (int i = 40; i < 80; i+=10)
        {
            for (int j = 1; j <= 10; j++)
            {
                str += i+j+" ";
            }
            str += "\n";
        }
        print(str);

        str = "";
        for (int i = 0; i < 4; i++)
        {
            for (int j = 0; j < 10; j++)
            {
                str += (40+(i*10) + j+1) + " ";
            }
            str += "\n";
        }
        print(str);
        */

        // ------ 20 -------
        // n=100
        // 100/64=1
        // 100-64=36
        // 36/32=1
        // 4/16=0
        // 4/8=0
        // 4/4=1

        /*int n = 500;  // Зарплата
        int kup = 6;  // Додатковий параметр для визначення номіналу   
        string str = "";  // Рядок для формування відповіді
        while(n>0)  // Рахуємо поки не видали всю зарплату
        {
            int nominal = (int)Mathf.Pow(2, kup); // Вираховуємо номінал
            int countKup = n / nominal;  // Кількість купюр
            if (countKup > 0)  // Якщо є що видати поточним номіналом
            {
                // Дописуємо відповідний запис: яких купюр та скільки
                str += nominal + ": " + countKup+"\n";
                // Рахуємо скільки ще віддати
                n -= nominal * countKup;
            }
            // Зменшуємо номінал купюри
            kup--;
        }
        // Відповідь на друк
        print(str);
        */

        // ---------- 16a ----------
        /*string str = "";
        for(int i=0; i<5; i++)
        {
            for(int j=0; j<i+1; j++)
            {
                str += (i+1) + " ";
            }
            str += "\n";
        }
        print(str);*/

        // ---------- 16b ----------
        /*string str = "";
        for (int i = 0; i < 5; i++)
        {
            for (int j = 0; j < 5-i; j++)
            {
                str += (i + 5) + " ";
            }
            str += "\n";
        }
        print(str);
        */




    }
   
}
