﻿using UnityEngine;

public class OOP2 : MonoBehaviour
{
    void Start()
    {
        /*Transport transport = new Transport(40, 2015, Transport.Palne.H2);
        transport.PrintInfo();*/

        /*Auto auto = new Auto(250, 2019, Transport.Palne.Kerosin, "opel", "VK 777 AA");
        auto.PrintInfo();
        print(auto.AvgSpeed);*/

        Transport[] trans = new Transport[3];
        trans[0] = new Auto(250, 2005, Transport.Palne.Dizel, "BMW", "333");
        trans[1] = new Poizd(100, 1983, Transport.Palne.Elektrika, 5, 60, 123);
        trans[2] = new Litak(900, 2017, Transport.Palne.Kerosin, 2000, 200);

        /*foreach(var t in trans)
        {
             t.PrintInfo();
            print("----------");
        }*/

        for (int i = 0; i < 3; i++)
        {
            if(trans[i] is Auto)
            {
                (trans[i] as Auto).PrintInfo();
            }
            else if(trans[i] is Poizd)
            {
                (trans[i] as Poizd).PrintInfo();
            }
            else if(trans[i] is Litak)
            {
                (trans[i] as Litak).PrintInfo();
            }
            print("-------------");
        }

    }

    
}
