﻿using UnityEngine;
using System;

class Test
{
    // поля
    public string name;
    public int id;
    private int _count;

    // властивості
    public int Count
    {
        // повертає значення закритого поля
        get
        {
            return _count;
        }
        // встановлює значення закритого поля
        set
        {
            _count = value;
        }
    }

    public float MaxSpeed { get;  }
     
    // конструктори
    public Test()
    {
        name = "noname";
        id = 0;
        _count = 0;
        MaxSpeed = 1;
        Debug.Log("Hello1");
    }

    public Test(string name, int id, int count, float maxSpeed)
    {
        this.name = name;
        this.id = id;
        Count = count;
        MaxSpeed = maxSpeed;
        Debug.Log("Hello2");
    }

    // деструктор
    ~ Test()
    {
        Debug.Log("pa-pa");
    }

    // методи
    public void PrintInfo()
    {
        Debug.LogFormat("Name: {0};  id: {1};  count: {2}", name, id, _count);
    }
       
}


class Human
{
    public Sex sex;
    //public string sex2;
    public string colorOfEyes;
    private float _height; // metr
    private float _masa; // kilogram
    private string _name;
    private int _yearOfBirth;

    public int Age
    {
        get
        {
            return DateTime.Today.Year - _yearOfBirth;
        }
    }

    public int YearOfBirth
    {
        get
        {
            return _yearOfBirth;
        }
        set
        {
            //_yearOfBirth = value >= 1900 ? value : 1900;
            if (value >= 1900)
            {
                _yearOfBirth = value;
            }
            else
            {
                _yearOfBirth = 1900;
                Debug.LogError("Некоректний рік народження");
            }
        }
    }

    public float Height
    {
        get
        {
            return _height;
        }
        set
        {
            if(value > 0)
            {
                _height = value;
            }
            else
            {
                Debug.LogError("Некоректний ріст");
            }
        }
    }

    public float Masa
    {
        get
        {
            return _masa;
        }
        set
        {
            if(value>0)
            {
                _masa = value;
            }
            else
            {
                Debug.LogError("Некоректна вага");
            }
        }
    }
    
    // конструктор
    public Human()
    {
        _name = "noname";
        _yearOfBirth = 1900;
        _masa = 1f;
        _height = 1f;
        sex = Sex.n_a;
        colorOfEyes = "black";
    }

    public Human(string name, Sex sex, int yearOfBirth, float masa, float height, string colorOfEyes)
    {
        _name = name;
        this.sex = sex;
        YearOfBirth = yearOfBirth;
        Masa = masa;
        Height = height;
        this.colorOfEyes = colorOfEyes;
    }

    public string StrInfo()
    {
        return "Name: " + _name + ";  sex: " + sex + ";  YearOfBirth: " + YearOfBirth + ";  Age: " + Age +
                ";  Masa: " + Masa + ";  Height: " + Height + ";  colorOfEyes: " + colorOfEyes;
    }

}

enum Sex
{
    Man,
    Woman,
    n_a
}


class Money
{
    private int _first;
    private int _second;

    public int First
    {
        get
        {
            return _first;
        }
        set
        {
            if(value>0)
            {
                switch(value)
                {
                    case 1:
                    case 2:
                    case 5:
                    case 10:
                    case 20:
                    case 50:
                    case 100:
                    case 200:
                    case 500:
                        {
                            _first = value;
                            break;
                        }
                    default:
                        {
                            _first = 1;
                            Debug.LogError("Nevirnyy nominal");
                            break;
                        }
                }
            }
            else
            {
                _first = 1;
                Debug.LogError("Nevirnyy nominal");
            }
        }
    }

    public int Second
    {
        get
        {
            return _second;
        }
        set
        {
            if(value>0)
            {
                _second = value;
            }
            else
            {
                Debug.LogError("Kilkist maje butu >0");
            }
        }
    }

    public Money()
    {
        _first = 1;
        _second = 1;
    }

    public Money(int first, int second)
    {
        First = first;
        Second = second;
    }

    public void Display()
    {
        Debug.LogFormat("Nominal: {0};  Kilkist: {1}", _first, _second);
    }

    public int Suma()
    {
        return _first * _second;
    }

}

struct ComplexNumber
{
    public float a;
    public float b;

    public ComplexNumber(float a, float b)
    {
        this.a = a;
        this.b = b;
    }

    public void Display()
    {
        Debug.LogFormat("({0}, {1})", a, b);
    }

    public string toString()
    {
        return "("+a+", "+b+")";
    }
}

class Complex
{

    public ComplexNumber Add(ComplexNumber complex1, ComplexNumber complex2 )
    {
        return new ComplexNumber(complex1.a+complex2.a, complex1.b+complex2.b);

    }
}
