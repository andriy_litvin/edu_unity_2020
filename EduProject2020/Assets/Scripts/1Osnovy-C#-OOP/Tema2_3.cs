﻿using UnityEngine;

public class Tema2_3 : MonoBehaviour
{
    public int m;

    void Start()
    {
        // чи більше за 10
        /*int number = Random.Range(1, 21); // 1..20
        print(number);
        if (number > 10)
        {
            Debug.LogWarning("> 10");
        }
        else
        {
            print("<=10");
        }*/

        // визнвчити яке число менше, більше або рівне 0
        /*int b = Random.Range(-10, 11); // -10..10
        if (b > 0)
        {
            Debug.LogFormat("{0} > 0", b);
        }
        else if (b == 0)
        {
            print("0 == 0");
        }
        else
        {
            Debug.LogFormat("{0} < 0", b);
        }*/

        // пошук максимального
        /*int number1 = Random.Range(1, 30); // 1..29
        int number2 = Random.Range(1, 30);
        int number3 = Random.Range(1, 30);

        Debug.LogFormat("{0}  {1}  {2}", number1, number2, number3);

        if (number1 >= number2)
        {
            if (number1 >= number3)
            {
                print("max: " + number1);
            }
            else
            {
                print("max: " + number3);
            }
        }
        else if (number2 >= number3)
        {
            if (number2 >= number1)
            {
                print("max: " + number2);
            }
        }
        else
        {
            print("max: " + number3);
        }*/

        // 2a
        /*int x = 2;
        int y = 2;
        bool b = (x >= 0) && (y * y > 4);
        print("zav. 2a: " + b);*/

        //4b
        /*int number = 122;
        int num1 = number / 100;
        int num2 = (number % 100) / 10;
        int num3 = number % 10;

        if (num1 == num2 || num2 == num3 || num3 == num1)
        {
            print("min 2 odnakovi");
        }*/

        // дні тижня
        /*int day = Random.Range(1, 9);
        print("day: " + day);
        switch (day)
        {
            case 1:
                print("ponedilok");
                break;
            case 2:
                print("vivtorok");
                break;
            case 3:
                print("sereda");
                break;
            case 4:
                print("chetver");
                break;
            case 5:
                print("pjatnycja");
                break;
            case 6:
                print("subota");
                break;
            case 7:
                print("nedilja");
                break;
            default:
                print("takoho dnja nema");
                break;
        }*/


        // пори року
        /*int m = Random.Range(1, 13);
        print("m = " + m);
        switch (m)
        {
            case 12:
            case 1:
            case 2:
                print("zuma");
                break;
            case 3:
            case 4:
            case 5:
                print("vesna");
                break;
            case 6:
            case 7:
            case 8:
                print("lito");
                break;
            case 9:
            case 10:
            case 11:
                print("osin");
                break;
            default:
                print("tokoho misaca nema");
                break;
        }*/

        //4.52
        /*int a = Random.Range(10, 30);
        int b = Random.Range(10, 40);
        int d = Random.Range(15, 50);

        Debug.LogFormat("{0} x {1} | {2}", a, b, d);
        if (a >= d + 2 && b >= d + 2)
        {
            print("Vasi povezlo");
        }
        else
        {
            print("Vasi ne povezlo");
        }*/

        // парне поділити на 2 не парне помножити на 2
        /*int num = Random.Range(1,11);
        print("num: " + num);

        // 5 = 2 2 1
        // 6 = 2 2 2 

        if (num % 2 == 0)
        {
            num /= 2;
        }
        else
        {
            num *= 2;
        }
        print(num);

        num = num % 2 == 0 ? num / 2 : num * 2;
        */

        // ------ 7 -------
        /*int n = 1800;

        if ((n % 100 != 0 && n % 4 == 0) || (n % 100 == 0 && n % 400 == 0))
        {
            print("vysokosny");
        }
        else
        {
            print("ne vysokosnyy");
        }*/

        // ----- 9 -----
        /*int m = 301;  // Загальна кількість місяців
        int year = m / 12; // Кількість повних років
        int month = m % 12; // Залишок місяців

        // 1, 21, 31 - rik
        // 2, 3, 4, 22, 23 - roky
        // 5, 6, 7, 8, 9, 10...20 - rokiv

        int yearZ; // допоміжна змінна
        // якщо рік >=5 i <=20 то в допоміжну змінну пишемо ті роки, що є
        if (year >= 5 && year <= 20)  
        {
            yearZ = year;   
        }
        // інакше знаходимо цифру на яку закінчується рік
        else
            yearZ = year % 10;
        
        switch(yearZ)
        {
            case 1:
                print(year + "rik");
                break;
            case 2:
            case 3:
            case 4:
                print(year + "roky");
                break;
            default:
                print(year + "rokiv");
                break;
        }*/

        // 4.56a
        /*int a = 27;
        int b = a / 10;  // 2
        int c = a % 10;  // 3

        if (b == 4 || b == 7 || c == 4 || c == 7)
        {
            print("v chislo vhodyt 4 abo 7");
        }
        else
            print("v chislo ne vhodyt 4 abo 7");
*/

        // 4.57b
        /*int a = 732;
        int a1 = a / 100; // 7
        int a2 = (a % 100) / 10; // 3
        int a3 = a % 10;  // 2
        int n = Random.Range(0, 10); // 0..9

        if (a1 == n || a2 == n || a3 == n)
        {
            print("znayshly");
        }
        else
            print("nema");

        */

        // 12
        // 9 - 1001
        //     1011 - 11

        int n = 9;
        int i = 2;
        int b = 1 << i;
        n = n | b;
        print(n);

    }


}
