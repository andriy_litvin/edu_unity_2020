﻿using UnityEngine;

class Person
{
    public string name;
    public int age;

    public Person(string name, int age)
    {
        this.name = name;
        this.age = age;
    }

    public virtual void PrintInfo()
    {
        Debug.LogFormat("Персона: {0}; Вік: {1}", name, age);
    }

}

class Student: Person
{
    public string highSchoolName;

    public Student(string highSchoolName, string name, int age):base(name, age)
    {
        this.highSchoolName = highSchoolName;
    }

    public override void PrintInfo()
    {
        Debug.LogFormat("Студент: {0}; Вік: {1}; ВНЗ: {2}", name, age, highSchoolName);
    }
}

class Pupil: Person
{
    public string schoolName;

    public Pupil(string schoolName, string name, int age):base(name, age)
    {
        this.schoolName = schoolName;
    }

    public override void PrintInfo()
    {
        Debug.LogFormat("Учень: {0}; Вік: {1}; Школа: {2}", name, age, schoolName);
    }
}


abstract class Animal
{
    public string name;
    public string type;

    public abstract void PrintInfo(); // Оголошення абстрактного методу

}

class Parrot: Animal
{
    public bool isSpeacker; // ознака - чи говорить

    public Parrot(string name, string type, bool isSpeacker)
    {
        this.name = name;
        this.type = type;
        this.isSpeacker = isSpeacker;
    }

    public override void PrintInfo()
    {
        Debug.LogFormat("Папуга: {0}; Вид: {1}; Говорить: {2}", name, type, isSpeacker ? "Так": "Ні");
    }
}

class Fox: Animal
{
    public bool isPisec;

    public Fox(string name, string type, bool isPisec)
    {
        this.name = name;
        this.type = type;
        this.isPisec = isPisec;
    }

    public override void PrintInfo()
    {
        Debug.LogFormat("Лисиця: {0}; Вид: {1}; isPisec: {2}", name, type, IsTrue());
    }

    private string IsTrue()
    {
        if (isPisec)
        {
            return "Так";
        }
        else
        {
            return "Ні";
        }
    }
}

