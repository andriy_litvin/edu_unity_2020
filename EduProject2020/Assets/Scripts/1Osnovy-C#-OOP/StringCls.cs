﻿using UnityEngine;

public class StringCls : MonoBehaviour
{
    void Start()
    {
        /*
        string str = "Hello Unity";
        print(str);
        str += "!!!"; // Hello Unity!!!
        str = "C# " + str; // C# Hello Unity!!!
        
        print(str[3]); // H  // тільки для читання

        print(str.Length); // 17

        //   \t - табуляція
        //   \n - перехід на новий рядок
        //   \\ - пише \
        //   \" - пише "  
        str = "Hello\t\"Unity\"\nHello\\C#";
        // Hello    "Unity"
        // Hello\C#


        string s1 = null, s2 = "", s3 = "Hello";
        string.IsNullOrEmpty(s1); // true
        string.IsNullOrEmpty(s2); // true
        string.IsNullOrEmpty(s3); // false

        print(string.IsNullOrEmpty("")); // true
        print(string.IsNullOrWhiteSpace("")); // true

        print(string.IsNullOrEmpty("\t")); // false
        print(string.IsNullOrWhiteSpace("\t")); // true

        str = "Hello";
        print(str.ToLower()); // hello
        print(str.ToUpper()); // HELLO

        str = "Hello Unity Hello World";
        string strFind = "Unity";
        print(str.Contains(strFind)); // true
        strFind = "aaa";
        print(str.Contains(strFind)); // false

        str = "Hello Unity Hello World";
        int index = -1;
        do
        {
            index = str.IndexOf("o", ++index);
            print(index);
        } while (index!=-1);

        //print(str.IndexOf("o")); // 4

        str = "Hello Unity Hello World";
        str = str.Insert(11, ",");
        print(str); // Hello Unity, Hello World

        str = "Hello Unity Hello World";
        print(str.Remove(11)); // Hello Unity
        print(str.Remove(0, 6)); // Unity Hello World

        str = "Hello Unity Hello World";
        print(str.Substring(12)); // Hello World
        print(str.Substring(6, 5)); // Unity

        str = "Hello Unity";
        char[] arr = str.ToCharArray();
        for (int i = 0; i < arr.Length; i++)
        {
            print(arr[i]);
        }

        str = "Arsenal, Milan, Real Madrid, Barcelona";
        string[] arrStr = str.Split(',');
        foreach (string s in arrStr)
        {
            print(s);
        }
        */

        // -- 1 --
        /*string str = "prohrama";
        string str1 = str.Substring(1, 2) + str.Substring(6,1); // rom
        string str2 = str.Remove(0,4); // rama
        print(str1);
        print(str2);
        */

        // -- 3 --
        /*string str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
        int count = 0;
        for (int i = 0; i < str.Length; i++)
        {
            if(str[i]==' ')
            {
                count++;
            }
        }
        print(count);
        */

        // -- 4 --
        /*string str = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua";
        string hol = "oaeiuyOAEIUY";
        int count = 0;
        for (int i = 0; i < str.Length; i++)
        {
            for (int j = 0; j < hol.Length; j++)
            {
                if (str[i] == hol[j])
                {
                    count++;
                }
            }
        }
        print(count);
        */

        // ----- 13 -----
        // 1-9
        // 11-19
        // 10 - 90
        string[] odynyci = {"", "один", "два", "три", "чотири", "п'ять", "шість", "сім", "вісім", "девять" };
        string[] desatki = {"", "десять", "двадцять", "тридцять","сорок","п'ятдесят","шістдесят","сімдесят","вісімдесят","девяносто"};
        int n = Random.Range(1, 30);
        print("n= " + n);
        string propys = "";

        if (n < 10) // 1-9
        {
            propys = odynyci[n];
        }
        else if (n == 10) // 10
        {
            propys = desatki[1];
        }
        else if (n < 20) // 11-19
        {
            propys = odynyci[n % 10] + "надцять";
            if (n == 14)
            {
                propys = propys.Remove(5, 1);
            }
        }
        else // 20-99
        {
            propys = desatki[n / 10] + " " + odynyci[n % 10];
        }
        print(propys);


    }

  
}
