﻿using UnityEngine;

public class Tema2 : MonoBehaviour
{
    /*public int i;
    public float f;
    public bool bo;
    public string str;
    */

    // Start is called before the first frame update
    void Start()
    {
        /*
        int a=0;
        float b = 2.5f;
        char c = '+';
        string s = "Hello";
        bool d = false; // true    


        print("a = " + a);
        print("b = " + b);
        print("c = " + c);
        print("s = " + s);
        print("d = " + d);

        print(i);
        print(f);
        print(bo);
        print(str);

        Debug.LogFormat("{0} {1} {2} {3}",i,f,bo,str);
        Debug.LogWarning("LogWarning!!!");
        Debug.LogError("LogError");
        */
        //print(i +""+ f + bo + str);

        // однорядковий коментар
        /*
         багаторядковий
         коментар       
        */
        /*
        const float pi = 3.1428f;
        a = a + 2;
        b = pi + 2;
        print(a);
        print(b);
        */

        //var val = "aaa";
        /*
        int a = 2;
        float f = 3f;
        int r = (int) (a + f);
        print(r); // 5
        float rf = a - f; // -1
        print(rf);
        r = a * 3; // 6
        print(r);

        rf = 5 / 2; // 2 
        print(rf); 
        rf = 5 / 2f; // 2.5 
        print(rf);

        r = 326 % 14; // 4
        print(r);
        */
        /*
        const float masa = 2500f;
        int s = (int)(masa / 1000);
        print(s);
        const int masa2 = 2500;
        int s2 = masa2 / 1000;
        print(s2);
        */


        int a = Random.Range(1, 10); // 1..9
        int b = Random.Range(1, 10);
        int c = Random.Range(1, 10);
        int x = Random.Range(1, 10);

        float rezul = 1 / Mathf.Sqrt( a*(x*x) + b*c + c );
        print(rezul);


        int m = 2;
        int m2 = m * m;
        int m4 = m2 * m2;
        int m6 = m2 * m4;
        print(m6);

    }


}
