﻿using UnityEngine;

class Transport
{
    private float _avgSpeed;
	private int _rikVupusku;
    public Palne typePalne;
    
    public float AvgSpeed
	{
		set
		{
            if(value>0)
			{
				_avgSpeed = value;
			}
			else
			{
				Debug.LogError("Некоректна швидкість");
			}
		}
		get
		{
			return _avgSpeed;
		}
	}

    public int RikVupusku
    {
        get
        {
            return _rikVupusku;
        }
        set
        {           
            if(value>=1900 && value <= System.DateTime.Today.Year)
            {
                _rikVupusku = value;
            }
            else
            {
                
                Debug.LogError("Некоректний рік випуску");
            }
        }
    }
    
    public enum Palne
    {
        Benzin,
        Dizel,
        Gas,
        Kerosin,
        Elektrika,
        H2,
        none
    }

    public Transport()
    {
        _avgSpeed = 1;
        _rikVupusku = 1900;
        typePalne = Palne.none;
    }

    public Transport(float avgSpeed, int rikVupusku, Palne palne)
    {
        AvgSpeed = avgSpeed;
        RikVupusku = rikVupusku;
        typePalne = palne;
    }

    public void PrintInfo()
    {
        Debug.LogFormat("Середня швидкість: {0}; Рік випуску: {1}; Пальне: {2}", _avgSpeed, _rikVupusku, typePalne);
    }

}

class Auto: Transport
{
    public string marka;
    public string nomer;

    public Auto()
    {
        marka = "ZAZ";
        nomer = "123";
    }

    public Auto(float avgSpeed, int rikVupusku, Palne palne, string marka, string nomer): base(avgSpeed, rikVupusku, palne)
    {
        this.marka = marka;
        this.nomer = nomer;
    }

    public void PrintInfo()
    {
        base.PrintInfo();
        Debug.LogFormat("Марка: {0}; Номер {1}", marka, nomer);
    }
}

class Poizd: Transport
{
    private int _kilkistVahoniv;
    private int _kilkistPasajirivVVahoni;
    public int nomer;

    public int KilkistVahoniv
    {
        get
        {
            return _kilkistVahoniv;
        }
        set
        {
            if(value>0)
            {
                _kilkistVahoniv = value;
            }
            else
            {
                Debug.LogError("Кількість вагонів не вірна");
            }
        }
    }

    public int KilkistPasajirivVVahoni
    {
        get
        {
            return _kilkistPasajirivVVahoni;
        }
        set
        {
            if(value>=10)
            {
                _kilkistPasajirivVVahoni = value;
            }
            else
            {
                Debug.LogError("Некоректна кількість пасажирів");
            }
        }
    }

    public Poizd()
    {
        _kilkistVahoniv = 1;
        _kilkistPasajirivVVahoni = 10;
        nomer = 1;
    }

    public Poizd(float avgSpeed, int rikVupusku, Palne palne, int kilkistVahoniv, int kilkistPasajirivVVahoni, int nomer): base(avgSpeed, rikVupusku, palne)
    {
        _kilkistVahoniv = kilkistVahoniv;
        _kilkistPasajirivVVahoni = kilkistPasajirivVVahoni;
        this.nomer = nomer;
    }

    public void PrintInfo()
    {
        base.PrintInfo();
        Debug.LogFormat("Номер: {0}; Кількість вагонів: {1}; Кількість пасажирів у вагоні: {2}", nomer, _kilkistVahoniv, _kilkistPasajirivVVahoni);
    }
       
}

class Litak: Transport
{
    public float dalnistPolotu; // km
    public int kilkistPasajiriv;

    public Litak()
    {
        dalnistPolotu = 1;
        kilkistPasajiriv = 2;
    }

    public Litak(float avgSpeed, int rikVupusku, Palne palne, float dalnistPolotu, int kilkistPasajiriv): base(avgSpeed, rikVupusku, palne)
    {
        this.dalnistPolotu = dalnistPolotu;
        this.kilkistPasajiriv = kilkistPasajiriv;
    }

    public void PrintInfo()
    {
        base.PrintInfo();
        Debug.LogFormat("Дальність польоту: {0}; Кількість пасажирів: {1}", dalnistPolotu, kilkistPasajiriv);
    }
}