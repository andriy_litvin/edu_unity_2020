using UnityEngine;

public class Arrays : MonoBehaviour
{
    //public int[] nums = { 8, 12, 15, 7, -5, 12, 15, 11 };

    void Start()
    {
        /*
        int[] a=new int[5];
        float[] b = new float[] {2f, 3.5f, 3.9f, -10.5f};
        string[] names = { "Vasya", "Petro", "Valera" };
        
        print(a[2]);
        a[2] = 8;
        print(a[2]);
        */

        // 1 2 3 4 5       
        /*for(int i=0; i<a.Length; i++)
        {
            a[i] = i + 1;
        }

        string str = "";
        for (int i = 0; i < a.Length; i++)
        {
            str += a[i] + " ";       
        }
        print(str);
        */

        // Заповнити масив з 7 ел. випадковим чином та порахувати суму елементів
        /*int[] nums = new int[7];
        for (int i = 0; i < nums.Length; i++)
        {
            nums[i] = Random.Range(0, 20); // 0..19
        }

        string str = "";
        for (int i = 0; i < nums.Length; i++)
        {
            str += nums[i] + " ";
        }
        print(str);

        int suma = 0;
        for(int i = 0; i < nums.Length; i++)
        {
            suma += nums[i];
        }
        print(suma);
        */

        // Порахувати суму парних елементів масиву
        /*int[] par = new int[50];
        for (int i = 0; i < par.Length; i++)
        {
            par[i] = Random.Range(0, 51); // 0..50
        }

        string str = "";
        foreach(int p in par)
        {
            str += p + " ";
        }
        print(str);

        int sumaPar = 0;
        foreach (int p in par)
        {
            if(p%2==0)
            {
                sumaPar += p;
            }
        }
        print(sumaPar);
        */

        // ---- 5a ----
        /*int numbersLen = Random.Range(10,100);
        int[] numbers = new int[numbersLen];

        for (int i = 0; i < numbersLen; i++)
        {
            numbers[i] = Random.Range(0, 1000);
        }

        string str = "";
        foreach (int p in numbers)
        {
            str += p + " ";
        }
        print(str);

        str = "";
        foreach (int p in numbers)
        {
            if(p>9 && p<100)
            {
                str += p + " ";
            }
        }
        print(str);
        */

        // ----- 12a -----
        //int[] nums = {8, 12, 15, 7, -5, 12, 15, 11};
        /*int max = int.MinValue;
        foreach (int i in nums)
        {
            if(i>max)
            {
                max = i;
            }
        }
        print("max = " + max);

        int countMax = 0;
        foreach(int i in nums)
        {
            if (i==max)
            {
                countMax++;
            }
        }
        print("count = " + countMax);
        */

        // ------- 14 -------
        /*int[] nums = new int[20];
        int temp;
        // Ознака запису нового значення
        bool isSet = false; 

        for(int i=0; i<20; )
        {
            temp = Random.Range(1, 40);
            isSet = true;
            for (int j = 0; j <= i; j++)
            {
                if (temp == nums[j])
                {
                    isSet = false;
                    break;
                }
            }
            if(isSet)
            {
                nums[i] = temp;
                i++;
            }
        }

        string str = "";
        foreach (int p in nums)
        {
            str += p + " ";
        }
        print(str);

        // Сортування        
        int min;
        int minPos;
        int numsLen = nums.Length;
        int jamp;

        for (int i = 0; i < numsLen - 1; i++)
        {
            min = nums[i];
            minPos = i;
            for (int j = i; j < numsLen; j++)
            {
                if (min > nums[j])
                {
                    min = nums[j];
                    minPos = j;
                }
            }
            jamp = nums[i];
            nums[i] = nums[minPos];
            nums[minPos] = jamp;            
        }

        str = "";
        foreach (int p in nums)
        {
            str += p + " ";
        }
        print(str);
        */

        /*
        int[] a = new int[9];
        int max = int.MinValue;
        int posMax=0;

        string str = "";
        for (int i = 0; i < a.Length; i++)
        {
            a[i] = Random.Range(-10,10);
            str += a[i] + " ";
        }
        print(str);

        for (int i = 0; i < a.Length; i++)
        {
            if (a[i] % 2 == 1 && a[i] > 0 && max < a[i])
            {
                max = a[i];
                posMax = i;
            }
        }
        Debug.LogFormat("a[{0}] = {1}", posMax, a[posMax]);
        */

        // ------- 10 64с -------
        float[] a = new float[7];
        float[] b = new float[9];
        float[] c = new float[a.Length + b.Length];

        for (int i = 0; i < 7; i++)
        {
            a[i] = Random.Range(1f, 20f);
        }

        for (int i = 0; i < 9; i++)
        {
            b[i] = Random.Range(1f, 20f);
        }

        for (int i = 0; i < 7; i++)
        {
            c[i] = a[i];
        }

        for (int i = 7; i < 16; i++)
        {
            c[i] = b[i-7];
        }

        string str = "";
        foreach(float i in a)
        {
            str += i + " ";
        }
        str += "\n";

        foreach (float i in b)
        {
            str += i + " ";
        }
        str += "\n";

        foreach (float i in c)
        {
            str += i + " ";
        }
        print(str);

        float max = float.MinValue;
        int posMax=0;
        float temp;

        for (int i = 0; i < 15; i++)
        {
            posMax = i;
            max = c[i];
            for (int j = i; j < 16; j++)
            {
                if(c[j]>max)
                {
                    max = c[j];
                    posMax = j;
                }
            }
            temp = c[i];
            c[i] = c[posMax];
            c[posMax] = temp;
        }

        str = "";
        foreach (float i in c)
        {
            str += i + " ";
        }
        print(str);

    }

}
