﻿using System.Collections.Generic;
using UnityEngine;

public class OOP3 : MonoBehaviour
{
    void Start()
    {
        /*List<Person> people = new List<Person>();
        people.Add(new Student("vodnik", "Ivan", 18));
        people.Add(new Pupil("ZOSH #5", "Olena", 15));
        people.Add(new Person("Oksana", 30));
        people.Add(new Person("Igor", 15));
        print(people.Count);
        
        foreach(var p in people)
        {
            p.PrintInfo();
        }

        // шукаємо в списку людей оксану та видаляємо її

        for(int i=0; i<people.Count; i++)
        {
            if(people[i].name=="Oksana")
            {
                people.Remove(people[i]);
                i--;
            } 
        }

        foreach (var p in people)
        {
            p.PrintInfo();
        }
        print(people.Count);*/

        Animal animal = new Fox("Alisa", "polarna", true);

        List<Animal> animals = new List<Animal>();
        animals.Add(new Parrot("Kesha", "kakadu", true));
        animals.Add(new Parrot("Sher", "jako", false));
        animals.Add(new Fox("Zoja", "western", false));
        animals.Add(new Fox("Valera", "forest", true));

        foreach(var a in animals)
        {
            a.PrintInfo();
        }
    }
       
}
