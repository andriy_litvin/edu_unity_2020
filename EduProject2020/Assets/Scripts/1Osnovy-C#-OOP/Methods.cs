﻿using UnityEngine;

public class Methods : MonoBehaviour
{
    void Start()
    {
        /*PrintHello("Ivan", 25); // позиційні
        PrintHello(age: 5, name: "Vasya"); // іменовані
        PrintHello("Olja"); // опціональні
        PrintHello(age:3);
        PrintHello();
        print(MaxValue(3,6));
        print(MaxValue(5.3f, 2.6f));*/

        /*int a=3;
        print(a); // 3
        Add(ref a);
        print(a); // 3

        float b;
        Add(out b);
        print(b);*/

        /*int num1 = 5;
        int num2 = 8;
        Debug.LogFormat("num1: {0}    num2: {1}", num1, num2);
        Change(ref num1, ref num2);
        Debug.LogFormat("num1: {0}    num2: {1}", num1, num2);
        */

        // ------ 2 ------
        /*string num = "1234";
        print(SumaNumbers(num));
        */

        // 3! = 1 * 2 * 3 
        //print(Factorial(5f));

        // ------ 8a -------
        //print(Progres(2,3,5));

        // ------ 8b ------
        //print(Progres2(2, 3, 5));

        print(SumaRek(1,5));


    }

    int SumaRek(int n, int m)
    {
        if(n==m)
        {
            return n;
        }
        else
        {
            return SumaRek(n, m - 1) + m;
        }
    }



    int Progres2(int n1, int delta, int n)
    {
        if (n == 1)
        {
            return n1;
        }
        else
        {
            return Progres2(n1, delta, n - 1) + n1 + delta * (n - 1);
        }
    }

    int Progres(int n1, int delta, int n)
    {
        if(n==1)
        {
            return n1;
        }
        else
        {
            return Progres(n1, delta, n - 1)+delta;
        }
    }



    int Factorial(int number)
    {
        print("factorial1 = " + number);
        if (number == 1)
        {
            print("factorial2 = 1");
            return 1;
        }
        else
        {
            int rezul = Factorial(number - 1) * number;
            print("factorial2 = " + rezul);
            return rezul;
        }
    }

    float Factorial(float number)
    {
        return number == 1 ? 1 : Factorial(number - 1) * number; 
    }


    int SumaNumbers(string number)
    {
        int suma = 0;
        for(int i=0; i<number.Length; i++)
        {
            suma += int.Parse(number.Substring(i,1)) ;
        }
        return suma;
    }



    void Change(ref int a, ref int b)
    {
        int temp=a;
        a = b;
        b = temp;
    }

    void Add(ref int a)
    {
        a++;
        print("met: " + a); //4
    }

    void Add(out float a)
    {
        a = 0; // ініціалізація
        a++;
        print("met: " + a); //4
    }


    void PrintHello(string name="n/a", int age=0)
    {        
        Debug.LogFormat("Hello {0} your {1} age", name, age);
    }

    int MaxValue(int a, int b)
    {
        if (a >= b)
        {
            return a;
        }
        else
        {
            return b;
        }
    }
    
    float MaxValue(float a, float b)
    {
        return a >= b ? a : b;
    }
    


}
