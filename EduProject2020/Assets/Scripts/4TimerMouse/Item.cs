﻿using UnityEngine;

public class Item : MonoBehaviour
{
    [SerializeField]
    private Sprite[] spritesItem;       // масив спрайтів
    private int id = -1;                // ідентифікатор
    private SpriteRenderer sr;
    private Vector2 marker1, marker2;
    private float timer;
    private float timerBlink = 1.5f;    // таймер блимання зірки
    private bool isRed = false;         // ознака на червону зірку
    private bool isFreez = false;       // ознака на заморозку

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        RandomItem();
        timer = Random.Range(2f, 4f);
    }

    void Update()
    {
        if (!isFreez)
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                RandomItem();
                RandomPosition();
                timer = Random.Range(2f, 4f);
            }

            if (id == 0)
            {
                timerBlink -= Time.deltaTime;
                if (timerBlink < 0)
                {
                    timerBlink = 1.5f;
                    isRed = !isRed;
                    if (isRed)
                        sr.color = Color.red;
                    else
                        sr.color = Color.white;
                }
            }
        }
    }

    void RandomItem()
    {
        int randomId;
        do
        {
            randomId = Random.Range(0, spritesItem.Length);
        } while (randomId==id);

        id = randomId;
        sr.sprite = spritesItem[id];
        sr.color = Color.white;
        isRed = false;


        if (id == 0)
        {
            timerBlink = 1.5f;
        }

    }

    void RandomPosition()
    {
        transform.position = new Vector3(Random.Range(marker1.x, marker2.x), Random.Range(marker1.y, marker2.y), 0);
    }

    public void SetMarker(Vector3 marker1, Vector3 marker2)
    {
        this.marker1 = new Vector2(marker1.x, marker1.y); 
        this.marker2 = new Vector2(marker2.x, marker2.y);
    }

    private void OnMouseDown()
    {
        if (id == 0)  // це зірка
        {
            if (isRed)   // червона зірка
            {
                InitGame.score -= 3;
                Freez();
            }
            else   // звичайна зірка
            {
                InitGame.score += 3;
                Destroy(gameObject);
            }
        }
        else   // інший об'єкт
        {
            InitGame.score -= 2;
            Destroy(gameObject);
        }
    }

    void Freez()
    {
        isFreez = true;
        sr.color = new Color(1, 0, 0, 0.4f);
        GetComponent<BoxCollider2D>().enabled = false;
    }
}
