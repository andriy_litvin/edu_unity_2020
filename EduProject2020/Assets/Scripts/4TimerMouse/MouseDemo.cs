﻿using UnityEngine;

public class MouseDemo : MonoBehaviour
{
    
    private void OnMouseEnter()
    {
        print("OnMouseEnter");
    }

    private void OnMouseExit()
    {
        print("OnMouseExit");
    }

    private void OnMouseOver()
    {
        print("OnMouseOver");
    }

    private void OnMouseDown()
    {
        print("OnMouseDown");
    }

    private void OnMouseUp()
    {
        print("OnMouseUp");
    }

    private void OnMouseDrag()
    {
        print("OnMouseDrag");
    }

    private void OnMouseUpAsButton()
    {
        print("OnMouseUpAsButton");
    }         
}
