﻿using UnityEngine;

public class Init1 : MonoBehaviour
{
    public int countSprite = 5;      // кількість спрайтів
    public Sprite1 spritePrefab;     // оригінал префаба для створення
    public float deltaPosX = 0.2f;   // відстань між спрайтами
    private Sprite1[] sprites;       // масив створених спрайтів
    private int selectId = 0;        // ідентифікатор виділеного спрайту

    void Start()
    {
        sprites = new Sprite1[countSprite];
        for (int i = 0; i < countSprite; i++)
        {
            sprites[i] = Instantiate(spritePrefab, new Vector3(i * deltaPosX, 0, 0), Quaternion.identity);
            sprites[i].Set(i, this);
        }
    }

    // перефарбовування спрайтів
    public void SelectedSprite(int id)
    {
        selectId = id;
        sprites[id].sr.color = Color.red;
        if (id - 1 >= 0)
        {
            sprites[id - 1].sr.color = Color.green;
        }
        if (id + 1 < countSprite)
        {
            sprites[id + 1].sr.color = Color.green;
        }
    }

    // фарбує спрайти в білий колір
    public void UnSelectedSprite()
    {
        for (int i = selectId - 1; i <= selectId + 1; i++)
        {
            if (i >= 0 && i < countSprite)
            {
                sprites[i].sr.color = sprites[i].startColor;    
            }
        }
    }

}
