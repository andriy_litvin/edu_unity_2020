﻿using UnityEngine;

public class TimerDemo : MonoBehaviour
{
    // таймер працює кожні 2 секунди
    /*float timer = 0;

    void Update()
    {
        timer += Time.deltaTime;
        if (timer > 2)
        {
            print("пройшло 2 с.");
            timer = 0;
        } 
    } */

    // таймер працює один раз через 2 секунди і вимикається
    float timer = 0;
    bool isTimer = true;

    void Update()
    {
        if (isTimer)
        {  
            timer += Time.deltaTime;
            if (timer > 2)
            {
                print("пройшло 2 с.");
                isTimer = false;

            }
        }
    }

}
