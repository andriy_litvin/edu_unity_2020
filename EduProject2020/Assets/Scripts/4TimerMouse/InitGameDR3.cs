﻿using UnityEngine;

public class InitGameDR3 : MonoBehaviour
{
    public Bagel badetPref;  // посилання на префаб пончика
    public Transform marker1, marker2;  // маркери для обмеження області створення пончиків
    public int countSprites;    // кількість спрайтів, що потрібно створити

    void Start()
    {
        for (int i = 0; i < countSprites; i++)
        {
            Bagel temp = Instantiate(badetPref,
                new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                            Random.Range(marker1.position.y, marker2.position.y), 0),
                            Quaternion.identity, transform);
            temp.SetMarker(marker1.position, marker2.position);

        }            
    }      
}
