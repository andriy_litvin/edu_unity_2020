﻿using UnityEngine;

public class Bagel : MonoBehaviour
{
    public float speed = 5;
    private SpriteRenderer sr;   // посилання на компонент SpriteRenderer
    private bool isRed;          // ознака на червоний колір
    private float timer;         // таймер на зміну кольору
    private Vector2 marker1, marker2;
    private Vector3 targetPoint;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        SetRandomColorSprite();
        timer = Random.Range(1f, 3f);
        targetPoint = new Vector3(Random.Range(marker1.x, marker2.x), Random.Range(marker1.y, marker2.y), 0);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            ChangeColor();
            timer = Random.Range(1f, 3f);
        }

        if (Vector3.Distance(transform.position, targetPoint) > 0.01)
        {
            transform.position = Vector3.MoveTowards(transform.position, targetPoint, speed * Time.deltaTime);
        }
        else
        {
            targetPoint = new Vector3(Random.Range(marker1.x, marker2.x), Random.Range(marker1.y, marker2.y), 0);
        }                 
    }

    void SetRandomColorSprite()
    {
        int random = Random.Range(0, 2);  // 0 1
        if (random == 0)
        {
            isRed = false;
            sr.color = Color.green;
        }
        else
        {
            isRed = true;
            sr.color = Color.red;
        }
    }

    void ChangeColor()
    {
        //sr.color = isRed ? Color.green : Color.red;
        if (isRed)
            sr.color = Color.green;
        else
            sr.color = Color.red;

        isRed = !isRed;
    }

    private void OnMouseDown()
    {
        if (isRed)
        {
            Freez();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    void Freez()
    {
        sr.color = new Color(1, 1, 1, 0.4f);
        this.enabled = false;
    }

    public void SetMarker(Vector3 marker1, Vector3 marker2)
    {
        this.marker1 = marker1;
        this.marker2 = marker2;
    }
}
