﻿using UnityEngine;

public class InitGame : MonoBehaviour
{
    public static int score = 0;   // рахунок
    public int countStart = 3;     // кількість спрайтів на старті гри
    public float timerGame = 30;   // таймер тривалості гри
    [SerializeField]
    private Item itemPref;         // посилання на префаб Item
    [SerializeField]
    private Transform marker1, marker2;   // точки обмеження ігрової області
    private float timer;           // таймер для створення нових спрайтів

    void Start()
    {
        for (int i = 0; i < countStart; i++)
        {
            AddItem(); 
        }
        timer = Random.Range(1f, 2f);
    }

    void Update()
    {
        timer -= Time.deltaTime;
        if (timer < 0)
        {
            AddItem();
            timer = Random.Range(1f, 2f);
        }

        timerGame -= Time.deltaTime;
        if (timerGame < 0)
        {
            print("GAME OVER\nScore: " + score);
            Destroy(gameObject);
        }
    }

    // метод створення нового об'єкта
    void AddItem()
    {
        Item temp = Instantiate(itemPref,
            new Vector3(Random.Range(marker1.position.x, marker2.position.x),
                    Random.Range(marker1.position.y, marker2.position.y), 0), 
                    Quaternion.identity, transform);
        temp.SetMarker(marker1.position, marker2.position);
    }
}
