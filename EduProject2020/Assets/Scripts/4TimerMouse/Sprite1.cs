﻿using UnityEngine;

public class Sprite1 : MonoBehaviour
{
    public SpriteRenderer sr; // посилання на компонент SpriteRenderer
    private int id;           // ідентифікатор спрайта
    private Init1 init;       // посилання на керуючий (основний) скрипт
    public Color startColor;

    void Awake()
    {
        sr = GetComponent<SpriteRenderer>();
        startColor = sr.color;
    }

    public void Set(int id, Init1 init)
    {
        this.id = id;
        this.init = init;
    }

    private void OnMouseEnter()
    {
        init.SelectedSprite(id);
    }

    private void OnMouseExit()
    {
        init.UnSelectedSprite();
    }

}
