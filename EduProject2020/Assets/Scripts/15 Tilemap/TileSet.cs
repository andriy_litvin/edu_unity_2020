﻿using UnityEngine.Tilemaps;
using UnityEngine;

public class TileSet : MonoBehaviour
{
    public TileBase tileSet;
    Tilemap map;

    void Start()
    {
        map = GetComponent<Tilemap>();
        
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 worldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3Int cellPos = map.WorldToCell(worldPoint);
            //print(cellPos);
            cellPos.z = 0;
            map.SetTile(cellPos, tileSet);
            
        }
    }
}
