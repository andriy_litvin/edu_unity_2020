﻿using UnityEngine;

public class ListenerColor : MonoBehaviour
{
    MeshRenderer ren;

    void Awake()
    {
        ren = GetComponent<MeshRenderer>();
        Messenger<Color>.AddListener(GameEvent.SEND_COLOR, OnSetColor);
    }

    void OnDestroy()
    {
        Messenger<Color>.RemoveListener(GameEvent.SEND_COLOR, OnSetColor);   
    }

    void OnSetColor(Color col)
    {
        ren.material.color = col;
    }
}
