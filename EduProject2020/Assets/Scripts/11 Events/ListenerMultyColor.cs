﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListenerMultyColor : MonoBehaviour
{
    int id;
    MeshRenderer ren;
    
    void Awake()
    {
        id = gameObject.GetInstanceID();
        ren = GetComponent<MeshRenderer>();
        Messenger<Dictionary<int, Color>>.AddListener(GameEvent.SEND_MULTY_COLOR, OnSetColor);
    }

    private void Start()
    {
        Messenger<int, Fiatures>.Broadcast(GameEvent.SEND_ID, id, Fiatures.add);         
    }

    private void OnDestroy()
    {
        Messenger<int, Fiatures>.Broadcast(GameEvent.SEND_ID, id, Fiatures.remove, MessengerMode.DONT_REQUIRE_LISTENER);
        Messenger<Dictionary<int, Color>>.RemoveListener(GameEvent.SEND_MULTY_COLOR, OnSetColor);
    }
               
    void OnSetColor(Dictionary<int, Color> color)
    {
        ren.material.color = color[id];
    }
}
