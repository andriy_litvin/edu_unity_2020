﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SendMultyColor : MonoBehaviour
{
    public List<int> objectsID;

    void Awake()
    {
        Messenger<int, Fiatures>.AddListener(GameEvent.SEND_ID, OnAddObjectId); 
    }                                                      

    void OnDestroy()
    {
        Messenger<int, Fiatures>.RemoveListener(GameEvent.SEND_ID, OnAddObjectId);

    }

    void OnAddObjectId(int id, Fiatures fiatures)
    {
        if (Fiatures.add == fiatures)
            objectsID.Add(id);
        else if (Fiatures.remove == fiatures)
            objectsID.Remove(id);
    }

    private void OnMouseDown()
    {
        Dictionary<int, Color> colors = new Dictionary<int, Color>();

        foreach (var item in objectsID)
        {
            colors.Add(item, new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f)));
        }

        Messenger<Dictionary<int, Color>>.Broadcast(GameEvent.SEND_MULTY_COLOR, colors);

    }
}

public enum Fiatures
{
    add,
    remove
}
