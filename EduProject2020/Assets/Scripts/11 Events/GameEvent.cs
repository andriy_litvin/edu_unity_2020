﻿public static class GameEvent 
{
    public const string SEND_EVENT = "SEND_EVENT";
    public const string SEND_MESS = "SEND_MESS";

    public const string SEND_COLOR = "SEND_COLOR";

    public const string SEND_ID = "SEND_ID";
    public const string SEND_MULTY_COLOR = "SEND_MULTY_COLOR";
}
