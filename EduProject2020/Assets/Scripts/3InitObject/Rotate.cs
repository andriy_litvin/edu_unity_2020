﻿using UnityEngine;

public class Rotate : MonoBehaviour
{     
    float x=0, y=0, z=0;

    private void Start()
    {
        x = Random.Range(-4f, 4f);
        y = Random.Range(-4f, 4f);
        z = Random.Range(-4f, 4f);
    }

    void Update()
    {
        transform.Rotate(x, y, z);
    }
}
