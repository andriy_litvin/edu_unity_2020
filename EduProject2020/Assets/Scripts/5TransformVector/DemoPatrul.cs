﻿using UnityEngine;

public class DemoPatrul : MonoBehaviour
{
    public Transform target;
    public float speed = 1;
    public float distancStop = 3;

    void Start()
    {
        
    }

    void Update()
    {
        transform.LookAt(target);
        if (Vector3.Distance(transform.position, target.position) > distancStop)
        {
            transform.Translate(Vector3.forward * speed * Time.deltaTime, Space.Self);
        }
    }
}
