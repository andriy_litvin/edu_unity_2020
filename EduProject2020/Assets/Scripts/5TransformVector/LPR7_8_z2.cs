﻿using UnityEngine;

public class LPR7_8_z2 : MonoBehaviour
{
    void Start()
    {
        GameObject go1 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        go1.transform.position = Vector3.zero;
        go1.name = "obj 1";

        GameObject[] go1Array = new GameObject[3];
        for (int i = 1; i <= 3; i++)
        {
            go1Array[i-1] = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            go1Array[i-1].transform.position = new Vector3(-20 + (10 * i), -2, 0);
            go1Array[i-1].name = "obj 1." + i;
            go1Array[i-1].transform.parent = go1.transform;
        }

        for (int i = 1; i <= 3; i++)
        {
            GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            temp.transform.position = new Vector3(-14 + (2 * i), -4, 0);
            temp.name = "obj 1.1." + i;
            temp.transform.parent = go1Array[0].transform;
        }

        for (int i = 1; i <= 4; i++)
        {
            GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            temp.transform.position = new Vector3(-5 + (2 * i), -4, 0);
            temp.name = "obj 1.2." + i;
            temp.transform.parent = go1Array[1].transform;
        }

        GameObject obj131 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        obj131.transform.position = new Vector3(10, -4, 0);
        obj131.name = "obj 1.3.1";
        obj131.transform.parent = go1Array[2].transform;

        GameObject obj1311 = AddObject(new Vector3(10, -6, 0), "obj 1.3.1.1", obj131.transform);
    }

    GameObject AddObject(Vector3 pos, string name, Transform parent)
    {
        GameObject newObj = GameObject.CreatePrimitive(PrimitiveType.Sphere);
        newObj.transform.position = pos;
        newObj.name = name;
        newObj.transform.parent = parent;
        return newObj;
    }
    
}
