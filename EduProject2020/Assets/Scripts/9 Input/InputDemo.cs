﻿using UnityEngine;

public class InputDemo : MonoBehaviour
{
    public Transform cube1, cube2;
    public float speed = 0.3f;
    float currentSpeed;
    string str="";

    void Start()
    {
        currentSpeed = speed; 
    }

    void Update()
    {
        /*if (Input.anyKey)
        {
            print("anyKey");
        }

        if (Input.anyKeyDown)
        {
            print("anyKeyDown");
        }*/

        /*str += Input.inputString;
        print(str);
        if (str.Contains("pererva"))
        {
            print("YRA!!!");
            str = "";
        } */

        //print(Input.mouseScrollDelta.ToString());

        //Debug.LogFormat("{0} | {1}", Input.GetAxis("Horizontal"), Input.GetAxisRaw("Horizontal"));
        //cube1.position += Vector3.right * Input.GetAxis("Horizontal") * speed;
        //cube2.position += Vector3.right * Input.GetAxisRaw("Horizontal") * speed;

        /*if (Input.GetMouseButton(0))
        {
            cube1.position += new Vector3(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"), 0)*speed;
        } */

        if (Input.GetKeyDown(KeyCode.LeftControl))
        {
            currentSpeed *= 2;
        }
        else if (Input.GetKeyUp(KeyCode.LeftControl))
        {
            currentSpeed = speed;
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            cube1.position += Vector3.right * currentSpeed;
        }
        else if (Input.GetKey(KeyCode.LeftArrow))
        {
            cube1.position += Vector3.left * currentSpeed;
        }
    }
}
