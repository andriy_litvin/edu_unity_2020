﻿using UnityEngine;

public class iTweenDemo : MonoBehaviour
{
    public Transform[] path;
    public Transform lookTarget;

    void Start()
    {
        //iTween.MoveAdd(gameObject, new Vector3(2, 1, 0), 2);
        //iTween.MoveAdd(gameObject, iTween.Hash("x", 4, "delay", 1, "time", 4, "easetype", iTween.EaseType.easeInOutSine));

        //iTween.MoveTo(gameObject, iTween.Hash("x",5,"time", 3, "easetype", iTween.EaseType.easeInOutCubic, "looptype", iTween.LoopType.pingPong));
        //iTween.MoveFrom(gameObject, new Vector3(3, 1, 1), 3);
        //iTween.MoveBy(gameObject, new Vector3(4, 1, 1), 4);

        //iTween.MoveAdd(gameObject, iTween.Hash("x", 4, "time", 4, "easetype", iTween.EaseType.easeInOutSine));
        //iTween.RotateAdd(gameObject, iTween.Hash("y", 90, "delay", 4, "time", 7, "easetype", iTween.EaseType.easeInElastic));
        //iTween.ScaleAdd(gameObject, iTween.Hash("amount", Vector3.one, "delay", 11, "time", 14, "easetype", iTween.EaseType.easeInBounce));

        //iTween.ShakePosition(gameObject, new Vector3(2, 0, 0), 3);

        //iTween.ColorTo(gameObject, Color.red, 3);

        //iTween.MoveTo(Camera.main.gameObject, iTween.Hash("path", path, "time", 15, "easetype", iTween.EaseType.linear, "looptype", iTween.LoopType.loop, "looktarget", lookTarget));

        iTween.MoveAdd(gameObject, iTween.Hash("x", 3, "time", 3, "oncomplete", "Finish", "oncompleteparams", 2));
    }



    void OnDrawGizmos()
    {
        iTween.DrawPath(path, Color.green);
    }

    void Finish(float timeDestroy)
    {
        print("Finish");
        Destroy(gameObject, timeDestroy);
    }
}
