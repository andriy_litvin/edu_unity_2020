﻿using UnityEngine;

public class BulletMario : MonoBehaviour
{
    // видалення кулі гравця при колізії з будь яким об'єктом
    private void OnCollisionEnter2D(Collision2D collision)
    {                                 
        Destroy(gameObject);           
    }

    // видалення кулі гравця при виході з області видимості всіх камер
    private void OnBecameInvisible()
    {
        Destroy(gameObject); 
    }

}
