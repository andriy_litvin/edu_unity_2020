﻿using UnityEngine;

public class MarioController : MonoBehaviour
{
    public float speed = 1;
    public Rigidbody2D prefabBullet;
    public float forseBulet=100;
    public Sprite spriteDead;
    Rigidbody2D rb;
    SpriteRenderer sr;
    bool isRight = true;
    Vector2 direction;
    int score = 0;
    int hp = 10;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        direction = Vector2.right;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            direction = Vector2.right;
            Flip();
        }
        else if (Input.GetKeyDown(KeyCode.A))
        {
            direction = Vector2.left;
            Flip();
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            direction = Vector2.up;
        }
        else if (Input.GetKeyDown(KeyCode.S))
        {
            direction = Vector2.down;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            Rigidbody2D newBullet = Instantiate(prefabBullet, transform.position, Quaternion.identity);
            newBullet.AddForce(direction * forseBulet);
        }

    }

    private void FixedUpdate()
    {
        rb.velocity = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"))*speed; 
    }

    void Flip()
    {
        if (direction == Vector2.left && isRight)
        {
            sr.flipX = true;
            isRight = false;
        }
        else if (direction == Vector2.right && !isRight)
        {
            sr.flipX = false;
            isRight = true;
        }
                     
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Bonus")
        {
            if (collision.name.Contains("Coin"))
            {
                score++;
            }
            else if (collision.name.Contains("Star"))
            {
                score += 5;
            }
            Destroy(collision.gameObject);
            print("Score: " + score);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "NPC")
        {
            hp -= 5;
            if (hp <= 0)
            {
                sr.sprite = spriteDead;
                rb.simulated = false;
                GetComponent<CapsuleCollider2D>().enabled = false;
                this.enabled = false;
                Time.timeScale = 0;

            }
        }
    }

}
