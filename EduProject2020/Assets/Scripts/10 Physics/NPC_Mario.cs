﻿using System.Collections.Generic;
using UnityEngine;

public class NPC_Mario : MonoBehaviour
{
    public float speed = 1;
    public LayerMask layerWall;
    Rigidbody2D rb;
    Vector2 direction;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        direction = Vector2.up;
    }

    void FixedUpdate()
    {
        if (direction == Vector2.up && Physics2D.OverlapCircle(transform.position + Vector3.up * 0.5f, 0.1f, layerWall))
        {
            Turn(); 
        }
        else if (direction == Vector2.down && Physics2D.OverlapCircle(transform.position + Vector3.down * 0.5f, 0.1f, layerWall))
        {
            Turn();
        }
        else if (direction == Vector2.left && Physics2D.OverlapBox(transform.position + Vector3.left * 0.5f, new Vector2(0.2f, 0.5f), 0, layerWall))
        {
            Turn();
        }
        else if (direction == Vector2.right && Physics2D.OverlapBox(transform.position + Vector3.right * 0.5f, new Vector2(0.2f, 0.5f), 0, layerWall))
        {
            Turn();
        }
        rb.velocity = direction * speed;
    }

    void Turn()
    {
        List<Vector2> dir = new List<Vector2> { Vector2.right, Vector2.left, Vector2.up, Vector2.down };
        dir.Remove(direction);

        for (int i = 0; i < dir.Count; i++)
        {
            if (Physics2D.OverlapCircle(transform.position + (Vector3)dir[i] * 0.5f, 0.1f, layerWall))
            {
                dir.RemoveAt(i);
                i--;
            }
        }

        if (dir.Count == 0)
        {
            Debug.LogError("Not direction");
            this.enabled = false;
        }

        direction = dir[Random.Range(0, dir.Count)];
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Destroy(collision.gameObject);
            Destroy(gameObject); 
        }
    }

}
