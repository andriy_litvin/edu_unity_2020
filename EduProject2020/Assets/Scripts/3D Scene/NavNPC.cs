﻿using System.Collections;
using UnityEngine;
using UnityEngine.AI;

public class NavNPC : MonoBehaviour
{
    public Transform[] area;
    NavMeshAgent agent;
    Animator anim;
    Vector3 pointPatrul;
    Transform player;
    bool isAttackWalk = false;
    bool isAttack = false;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        NextPointPatrul();
        player = PlayerController3d.player.transform;
    }

    void Update()
    {
        anim.SetFloat("speed", agent.velocity.magnitude);
        float distanc = Vector3.Distance(transform.position, player.position);
        if ( distanc < 15)
        {
            if (!isAttackWalk)
            {
                Debug.DrawRay(transform.position, player.position - transform.position, Color.red);
                RaycastHit hit;
                if (Physics.Raycast(transform.position, player.position - transform.position, out hit))
                {
                    if (hit.transform.tag == "Player")
                    {
                        isAttackWalk = true;
                    }
                }
            }
            else
            {
                agent.SetDestination(player.position);
                if (distanc < 2 && !isAttack)
                {
                    isAttack = true;
                    anim.SetTrigger("attack");
                }
            }
        }
        else if (isAttackWalk)
        {
            isAttackWalk = false;
            agent.SetDestination(player.position);
        }
        
    }

    void NextPointPatrul()
    {
        pointPatrul = Vector3.zero;
        for (int i = 0; i < 3; i++)
        {
            pointPatrul[i] = Random.Range(area[0].position[i], area[1].position[i]);
        }
        agent.SetDestination(pointPatrul);
        StartCoroutine(Stoped());
    }

    IEnumerator Stoped()
    {
        yield return new WaitForSeconds(1);

        while (true)
        {
            yield return null;
            if (agent.velocity.magnitude < 0.1)
            {
                agent.SetDestination(transform.position);
                yield return new WaitForSeconds(3);
                NextPointPatrul();
                break;
            }
        }
    }

    public void Damage()
    {
        PlayerController3d.player.AddHP(-20, transform.position);
    }

    public void EndAttack()
    {
        isAttack = false;
    }
}
