﻿using UnityEngine;

public class LoadSave1 : MonoBehaviour
{
    public int score = 0;
    
    public Transform cube;
    public SpriteRenderer ponchik;
    public Transform[] resObjects;

    void Start()
    {
        cube = Resources.Load<Transform>("Cube");
        ponchik = Resources.Load<SpriteRenderer>("obj1/Ponchik");
        resObjects = Resources.LoadAll<Transform>("");

        Instantiate(cube, new Vector3(-2, 0, 0), Quaternion.identity);
        Instantiate(ponchik, new Vector3(2, 0, 0), Quaternion.identity);

        if (PlayerPrefs.HasKey("score"))
        {
            score = PlayerPrefs.GetInt("score");
        }
        else
        {
            print("Ключ score в реєстрі не знайдено");
        }

        if (PlayerPrefs.HasKey("pos"))
        {
            string strPos = PlayerPrefs.GetString("pos");
            string [] arrayStr = strPos.Split(';');
            Vector3 tempPos=Vector3.zero;
            for (int i = 0; i < 3; i++)
            {
                tempPos[i] = float.Parse(arrayStr[i]);   
            }
            transform.position = tempPos;
        }     
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("score", score);

        string strPos = "";
        for (int i = 0; i < 3; i++)
        {
            strPos += transform.position[i]+";";
        }
        strPos = strPos.Remove(strPos.Length - 1);
        print(strPos);
        // 1;2;3;
        PlayerPrefs.SetString("pos", strPos);

    }

}
