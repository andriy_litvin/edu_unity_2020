﻿using UnityEngine;
using UnityEngine.UI;

public class UIPlatformer : MonoBehaviour
{
    public Image imageHP;
    public Image[] imgAllmaz;

    float maxHP = 20;
    Color colorDefault;

    private void Start()
    {
        colorDefault = imgAllmaz[0].color;
    }

    public void InitUI(int hp)
    {
        maxHP = hp;        
    }

    public void SetHP(int hp)
    {
        imageHP.fillAmount = hp / maxHP;
    }

    public void SetCountAlmaz(int countAlmaz)
    {
        for(int i=1; i<=3; i++)
        {
            if(i<=countAlmaz)
            {
                imgAllmaz[i - 1].color = Color.white;
            }
            else
            {
                imgAllmaz[i - 1].color = colorDefault;
            }
        }

    }


}
