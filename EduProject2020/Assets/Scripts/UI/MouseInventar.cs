﻿using UnityEngine;
using UnityEngine.UI;

public class MouseInventar : MonoBehaviour
{
    public static MouseInventar mouse;
    public Image img;
    public Text countText;
    public int count;
    public string nameItem;

    void Awake()
    {
        mouse = this;
        img = GetComponent<Image>();
        countText = GetComponentInChildren<Text>();
        img.color = new Color(1, 1, 1, 0);
        countText.text = "";
    }

    void Update()
    {
        transform.position = Input.mousePosition;
    }

    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count == 0 ? "" : count.ToString();
    }
}
