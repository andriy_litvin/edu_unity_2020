﻿using UnityEngine;

[ExecuteInEditMode]
public class GUIPlatformer : MonoBehaviour
{
    public Texture textureHP;
    public Texture textureActiveAlmaz;
    public Texture textureNotActiveAlmaz;
    float maxHP = 20;
    int hp = 20;
    int countAlmaz = 0;

    public void InitGUI(int hp)
    {
        maxHP = hp;
        this.hp = hp;
    }

    public void SetHP(int hp)
    {
        this.hp = hp;
    }

    public void SetCountAlmaz(int countAlmaz)
    {
        this.countAlmaz = countAlmaz;
    }

    private void OnGUI()
    {
        GUI.BeginGroup(new Rect(10, 10, 120, 60));
        GUI.Box(new Rect(10, 10, 100, 40), "");
        for (int i = 1; i <= 3; i++)
        {
            if (countAlmaz >= i)
            {
                GUI.DrawTexture(new Rect(10 + 10 * i + (20 * (i - 1)), 20, 20, 20), textureActiveAlmaz);
            }
            else
            {
                GUI.DrawTexture(new Rect(10 + 10 * i + (20 * (i - 1)), 20, 20, 20), textureNotActiveAlmaz);
            }

        }
        GUI.EndGroup();

        GUI.DrawTexture(new Rect(0, Screen.height - 20, hp/maxHP * Screen.width, 20), textureHP);   
    }
}
