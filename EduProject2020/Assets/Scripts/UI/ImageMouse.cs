﻿using UnityEngine.UI;
using UnityEngine;

public class ImageMouse : MonoBehaviour
{    
    public static ImageMouse mouse;
    public Image img;

    public Color alphaZero { get => new Color(1,1,1,0);  }


    void Awake()
    {
        mouse = this;
        img = GetComponent<Image>();
        img.color = alphaZero;
    }
        
    void Update()
    {
        transform.position = Input.mousePosition;    
    }
}
