﻿using UnityEngine;
using UnityEngine.UI;

public class ItemInventar : MonoBehaviour
{
    public Image img;
    public int count;
    public string nameItem;
    Text countText;
    MouseInventar mouse;
    
    void Start()
    {
        mouse = MouseInventar.mouse;
        countText = GetComponentInChildren<Text>();
        if (img.sprite)
        {
            countText.text = count.ToString();
        }
        else
        {
            img.color = new Color(1, 1, 1, 0);
            countText.text = "";
            count = 0;
        }
    }

    // початок перетягування
    public void BeginDrag()
    {
        print("BeginDrag");
        if (img.sprite && !mouse.img.sprite)
        {
            CellToMouse(); 
        }
    }

    public void EndDrag()
    {
        print("EndDrag");
        if (mouse.img.sprite)
        {
            MouseToCell();  
        }
    }

    public void Drop()
    {
        print("Drop");
        if (img.sprite)
        {
            SwapMouseCell();
        }
        else if (mouse.img.sprite)
        {
            MouseToCell();
        }
    }


    public void SetCount(int count)
    {
        this.count = count;
        countText.text = count==0 ? "" : count.ToString();
    }

    void CellToMouse()
    {
        mouse.img.sprite = img.sprite;
        mouse.img.color = new Color(1, 1, 1, 1);
        mouse.SetCount(count);
        img.color = new Color(1, 1, 1, 0);
        img.sprite = null;
        SetCount(0);
        mouse.nameItem = nameItem;
    }

    void MouseToCell()
    {
        img.sprite = mouse.img.sprite;
        mouse.img.color = new Color(1, 1, 1, 0);
        img.color = new Color(1, 1, 1, 1);
        mouse.img.sprite = null;
        SetCount(mouse.count);
        mouse.SetCount(0);
        nameItem = mouse.nameItem;
    }

    void SwapMouseCell()
    {
        Sprite tempSprite = img.sprite;
        int tempCount = count;
        SetCount(mouse.count);
        mouse.SetCount(tempCount);
        img.sprite = mouse.img.sprite;
        mouse.img.sprite = tempSprite;
        string tempName = nameItem;
        nameItem = mouse.nameItem;
        mouse.nameItem = tempName;
    }


}
