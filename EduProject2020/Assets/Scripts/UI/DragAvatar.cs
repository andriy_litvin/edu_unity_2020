﻿using UnityEngine.UI;
using UnityEngine;

public class DragAvatar : MonoBehaviour
{
    Image img;
    Image imgMouse;

    void Start()
    {
        img = GetComponent<Image>();
        imgMouse = ImageMouse.mouse.img;
    }

    public void BeginDrag()
    {
        imgMouse.sprite = img.sprite;
        img.color = ImageMouse.mouse.alphaZero;
        imgMouse.color = Color.white;
    }

    public void EndDrag()
    {
        if(imgMouse.sprite)
        {
            img.sprite = imgMouse.sprite;
            img.color = Color.white;
            imgMouse.color= ImageMouse.mouse.alphaZero;
            imgMouse.sprite = null;
        }
        else
        {
            Destroy(gameObject); 
        }
    }
         

}
