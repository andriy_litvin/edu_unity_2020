﻿using UnityEngine;

public class InventarSystem : MonoBehaviour
{
    public static InventarSystem sys;
    public ItemInventar[] elemInv;

    void Awake()
    {
        sys = this;
        elemInv = GetComponentsInChildren<ItemInventar>();
    }

    public void AddToInventar(Sprite sprite, string nameItem)
    {
        int countElemInv = elemInv.Length;
        for (int i = 0; i < countElemInv; i++)
        {
            if (elemInv[i].count > 0 && elemInv[i].nameItem == nameItem)
            {
                elemInv[i].SetCount(elemInv[i].count + 1);
                return;
            }
        }

        for (int i = 0; i < countElemInv; i++)
        {
            if (!elemInv[i].img.sprite)
            {
                elemInv[i].img.sprite = sprite;
                elemInv[i].SetCount(1);
                elemInv[i].nameItem = nameItem;
                elemInv[i].img.color = new Color(1, 1, 1, 1);
                return;
            }
        }
    }

}
