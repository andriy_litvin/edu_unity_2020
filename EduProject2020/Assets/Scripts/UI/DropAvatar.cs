﻿using UnityEngine.UI;
using UnityEngine;

public class DropAvatar : MonoBehaviour
{
    public RectTransform rectContent;
    Image img;
    Image imgMouse;

    void Start()
    {
        img = GetComponent<Image>();
        imgMouse = ImageMouse.mouse.img;
    }

    public void Drop()
    {
        if(imgMouse.sprite)
        {
            img.sprite = imgMouse.sprite;
            imgMouse.color = ImageMouse.mouse.alphaZero;
            imgMouse.sprite = null;
            /*Rect tempRect = rectContent.rect;
            print(tempRect.xMax);
            rectContent.rect.Set(tempRect.x, tempRect.y, tempRect.xMax+80, tempRect.yMax);*/

        }

    }
}
