﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;

public class SetDropDown : MonoBehaviour
{
    public GameObject panelItems;
    public Image imageAvatar;
    Dropdown dropdown;
    Image[] images; 

    void Start()
    {
        dropdown = GetComponent<Dropdown>();
        images = panelItems.GetComponentsInChildren<Image>();
        print(images.Length);

        dropdown.ClearOptions();

        List<Dropdown.OptionData> listSpr = new List<Dropdown.OptionData>();
        for (int i = 1; i < images.Length; i++)
        {
            listSpr.Add(new Dropdown.OptionData("Option "+i, images[i].sprite));
        }
        dropdown.AddOptions(listSpr);
    }

    public void SetAvatar()
    {
        imageAvatar.sprite = dropdown.options[dropdown.value].image;
    }

    
}
